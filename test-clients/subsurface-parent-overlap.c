#define _POSIX_C_SOURCE 200809L
#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <math.h>
#include <time.h>
#include <unistd.h>
#include <wayland-client.h>
#include <wayland-client-protocol.h>

#include "xdg-shell-client-protocol.h"
#include "viewporter-client-protocol.h"
#include "fractional-scale-v1-client-protocol.h"

struct context {
	struct wl_display *wl_display;

	/* Interfaces we need */
	struct wl_shm *wl_shm;
	struct wl_compositor *wl_compositor;
	struct wl_subcompositor *wl_subcompositor;
	struct xdg_wm_base *xdg_wm_base;
	struct wp_viewporter *wp_viewporter;
	struct wp_fractional_scale_manager_v1 *wp_fractional_scale_manager_v1; 

	/* Our instances of things */
	struct wl_surface *wl_surface;
	struct wp_fractional_scale_v1 *wp_fractional_scale_v1;

	struct wl_subsurface *wl_subsurfaces[1];
	struct wl_surface *wl_surfaces[1];

	/* Config */
	int surface_width;
	int surface_height;
	void (*draw_func)(uint32_t *, int, int, uint32_t);

	/* State */
	double scale;
	bool running;
};

static int scale_dimension(int pos, int dim, double scale) {
	return round((pos + dim) * scale) - round(pos * scale);
}

static int scale_position(int pos, double scale) {
	return round(pos * scale);
}

struct box {
	int x, y, width, height;
};

static void scale_box(struct box *box, double scale) {
	box->x = scale_position(box->x, scale);
	box->y = scale_position(box->y, scale);
	box->width = scale_dimension(box->x, box->width, scale);
	box->height = scale_dimension(box->y, box->height, scale);
}

static void randname(char *buf) {
	struct timespec ts;
	clock_gettime(CLOCK_REALTIME, &ts);
	long r = ts.tv_nsec;
	for (int i = 0; i < 6; ++i) {
		buf[i] = 'A'+(r&15)+(r&16)*2;
		r >>= 5;
	}
}

static int anonymous_shm_open(void) {
	char name[] = "/fractional-scale-XXXXXX";
	int retries = 100;

	do {
		randname(name + strlen(name) - 6);

		--retries;
		// shm_open guarantees that O_CLOEXEC is set
		int fd = shm_open(name, O_RDWR | O_CREAT | O_EXCL, 0600);
		if (fd >= 0) {
			shm_unlink(name);
			return fd;
		}
	} while (retries > 0 && errno == EEXIST);

	return -1;
}

static int create_shm_file(off_t size) {
	int fd = anonymous_shm_open();
	if (fd < 0) {
		return fd;
	}

	if (ftruncate(fd, size) < 0) {
		close(fd);
		return -1;
	}

	return fd;
}

static void xdg_surface_handle_configure(void *data,
		struct xdg_surface *xdg_surface, uint32_t serial) {
	struct context *ctx = data;
	xdg_surface_ack_configure(xdg_surface, serial);
	wl_surface_commit(ctx->wl_surface);
}

static const struct xdg_surface_listener xdg_surface_listener = {
	.configure = xdg_surface_handle_configure,
};

static void xdg_toplevel_handle_configure(void *data, struct xdg_toplevel
		*toplevel, int32_t width, int32_t size, struct wl_array *states) {
}

static void xdg_toplevel_handle_close(void *data,
		struct xdg_toplevel *xdg_toplevel) {
	struct context *ctx = data;
	ctx->running = false;
}

static const struct xdg_toplevel_listener xdg_toplevel_listener = {
	.configure = xdg_toplevel_handle_configure,
	.close = xdg_toplevel_handle_close,
};

static void handle_global(void *data, struct wl_registry *registry,
		uint32_t name, const char *interface, uint32_t version) {
	struct context *ctx = data;
	if (strcmp(interface, wl_shm_interface.name) == 0) {
		ctx->wl_shm = wl_registry_bind(registry, name, &wl_shm_interface, 1);
	} else if (strcmp(interface, wl_compositor_interface.name) == 0) {
		ctx->wl_compositor = wl_registry_bind(registry, name,
			&wl_compositor_interface, 4);
	} else if (strcmp(interface, wl_subcompositor_interface.name) == 0) {
		ctx->wl_subcompositor = wl_registry_bind(registry, name,
			&wl_subcompositor_interface, 1);
	} else if (strcmp(interface, xdg_wm_base_interface.name) == 0) {
		ctx->xdg_wm_base = wl_registry_bind(registry, name, &xdg_wm_base_interface, 2);
	} else if (strcmp(interface, wp_fractional_scale_manager_v1_interface.name)
			== 0) {
		ctx->wp_fractional_scale_manager_v1 = wl_registry_bind(registry, name,
				&wp_fractional_scale_manager_v1_interface, 1);
	} else if (strcmp(interface, wp_viewporter_interface.name) == 0) {
		ctx->wp_viewporter = wl_registry_bind(registry, name, &wp_viewporter_interface,
				1);
	}
}

static void handle_global_remove(void *data, struct wl_registry *registry,
		uint32_t name) {
	// Who cares
}

static const struct wl_registry_listener registry_listener = {
	.global = handle_global,
	.global_remove = handle_global_remove,
};

static void buffer_handle_release(void *data, struct wl_buffer *buffer) {
	wl_buffer_destroy(buffer);
}

static const struct wl_buffer_listener buffer_listener = {
	.release = buffer_handle_release,
};

static void draw_parent(uint32_t *pixels, int buffer_width, int buffer_height, struct box square, uint32_t inner_color, uint32_t outer_color) {
	for (int row = 0; row < buffer_height; row++) {
		for (int col = 0; col < buffer_width; col++) {
			bool inside = (col >= square.x && col < square.x + square.width) &&
				(row >= square.y && row < square.y + square.height);

			pixels[row * buffer_width + col] = inside ? inner_color : outer_color;
		}
	}
}

static void draw_solid(uint32_t *pixels, int buffer_width, int buffer_height, uint32_t argb_color) {
	for (int idx = 0; idx < buffer_width * buffer_height; idx++) {
		pixels[idx] = argb_color;
	}
}

static void* attach_buffer(struct context *ctx, struct wl_surface *wl_surface, int width, int height) {
	int stride = width * 4;
	int size = stride * height;

	int fd = create_shm_file(size);
	assert(fd > 0);
	void *shm_data = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	assert(shm_data != MAP_FAILED);

	struct wl_shm_pool *pool = wl_shm_create_pool(ctx->wl_shm, fd, size);
	struct wl_buffer *buffer = wl_shm_pool_create_buffer(pool, 0, width, height, stride, WL_SHM_FORMAT_ARGB8888);
	wl_buffer_add_listener(buffer, &buffer_listener, NULL);
	wl_shm_pool_destroy(pool);
	close(fd);

	// We won't flush until after we draw
	wl_surface_attach(wl_surface, buffer, 0, 0);
	wl_surface_damage_buffer(wl_surface, 0, 0, 0xFFFF, 0xFFFF);

	return shm_data;
}

static void render_everything(struct context *ctx) {
	int width = getenv("SCALED_PARENT_SURFACE")
		? scale_dimension(0, ctx->surface_width, ctx->scale)
		: ctx->surface_width;
	int height = getenv("SCALED_PARENT_SURFACE")
		? scale_dimension(0, ctx->surface_height, ctx->scale)
		: ctx->surface_height;

	void *shm_data = attach_buffer(ctx, ctx->wl_surface, width, height);
	struct box box = {
		.x = ctx->surface_width / 3,
		.y = ctx->surface_height / 3,
		.width = ctx->surface_width / 3,
		.height = ctx->surface_height / 3,
	};
	if (getenv("SCALED_PARENT_SURFACE")) {
		scale_box(&box, ctx->scale);
	}
	draw_parent(shm_data, width, height, box, 0xFF00FF00, 0xFF000000);
	munmap(shm_data, width * height * 4);

	width = getenv("SCALED_SUBSURFACES")
		? scale_dimension(ctx->surface_width / 3, ctx->surface_width / 3, ctx->scale)
		: ctx->surface_width / 3;
	height = getenv("SCALED_SUBSURFACES")
		? scale_dimension(ctx->surface_height / 3, ctx->surface_height / 3, ctx->scale)
		: ctx->surface_height / 3;
	shm_data = attach_buffer(ctx, ctx->wl_surfaces[0], width, height);
	draw_solid(shm_data, width, height, 0xFFFF0000);
	munmap(shm_data, width * height * 4);

	wl_surface_commit(ctx->wl_surfaces[0]);
}

static void surface_scale_handle_preferred_scale(void *data, struct
		wp_fractional_scale_v1 *info, wl_fixed_t scale) {
	struct context *ctx = data;
	ctx->scale = wl_fixed_to_double(scale);
	fprintf(stderr, "Preferred scale event: %lf\n", ctx->scale);

	render_everything(ctx);
	wl_surface_commit(ctx->wl_surface);
}

static const struct wp_fractional_scale_v1_listener surface_scale_listener = {
	.preferred_scale = surface_scale_handle_preferred_scale,
};

int main(int argc, char *argv[]) {
	struct context ctx = {
		.running = true,
		// Note: should be divisible by 3 due to 3x3 grid
		.surface_width = 150,
		.surface_height = 150,
		.scale = 1.0,
	};

	ctx.wl_display = wl_display_connect(NULL);
	assert(ctx.wl_display != NULL);

	struct wl_registry *registry = wl_display_get_registry(ctx.wl_display);
	wl_registry_add_listener(registry, &registry_listener, &ctx);
	wl_display_roundtrip(ctx.wl_display);

	assert(ctx.wl_shm != NULL);
	assert(ctx.wl_compositor != NULL);
	assert(ctx.xdg_wm_base != NULL);
	assert(ctx.wp_fractional_scale_manager_v1 != NULL);
	assert(ctx.wp_viewporter != NULL);

	ctx.wl_surface = wl_compositor_create_surface(ctx.wl_compositor);

	struct wp_viewport *wp_viewport = wp_viewporter_get_viewport(ctx.wp_viewporter, ctx.wl_surface);
	wp_viewport_set_destination(wp_viewport, ctx.surface_width, ctx.surface_height);

	ctx.wl_surfaces[0] = wl_compositor_create_surface(ctx.wl_compositor);
	ctx.wl_subsurfaces[0] = wl_subcompositor_get_subsurface(ctx.wl_subcompositor, ctx.wl_surfaces[0], ctx.wl_surface);
	wl_subsurface_set_position(ctx.wl_subsurfaces[0], ctx.surface_width / 3, ctx.surface_height / 3);

	wp_viewport = wp_viewporter_get_viewport(ctx.wp_viewporter, ctx.wl_surfaces[0]);
	wp_viewport_set_destination(wp_viewport, ctx.surface_width / 3, ctx.surface_height / 3);

	struct xdg_surface *xdg_surface= xdg_wm_base_get_xdg_surface(ctx.xdg_wm_base, ctx.wl_surface);
	struct xdg_toplevel *xdg_toplevel = xdg_surface_get_toplevel(xdg_surface);

	xdg_surface_add_listener(xdg_surface, &xdg_surface_listener, &ctx);
	xdg_toplevel_add_listener(xdg_toplevel, &xdg_toplevel_listener, &ctx);

	wl_surface_commit(ctx.wl_surface);
	wl_display_roundtrip(ctx.wl_display);

	ctx.wp_fractional_scale_v1 = wp_fractional_scale_manager_v1_get_fractional_scale(ctx.wp_fractional_scale_manager_v1, ctx.wl_surface);
	wp_fractional_scale_v1_add_listener(ctx.wp_fractional_scale_v1, &surface_scale_listener, &ctx);

	render_everything(&ctx);
	wl_surface_commit(ctx.wl_surface);

	while (wl_display_dispatch(ctx.wl_display) != -1 && ctx.running) {
		// This space intentionally left blank
	}

	wp_fractional_scale_v1_destroy(ctx.wp_fractional_scale_v1);
	xdg_toplevel_destroy(xdg_toplevel);
	xdg_surface_destroy(xdg_surface);
	wl_surface_destroy(ctx.wl_surface);

	return EXIT_SUCCESS;
}
