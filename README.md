# Fractional scaling

This repo contains test clients and documentation related to work on wayland fractional scaling, at the current time focused on the [wp-fractional-scale-v1](https://gitlab.freedesktop.org/wayland/wayland-protocols/-/merge_requests/143) proposal.

## Test clients

All the test clients focus on behavior with wp-fractional-scale-v1.

### single-surface

Creates a single surface that is fractionally scaled and shows a calibration grid with 1px features of interchangably white and black lines.

This can be used to see if the surface ends up with good alignment between buffer pixels and physical pixels.

### subsurface-adjacency

Creates a surfaces that is fractionally scaled with fully white content. A 3x3 grid of adjacent subsurfaces covering the entire main surfaces is shown above. This can be used to see if subsurface positioning maintains positional accuracy in spite of pixel alignment.

The default behavior is that the subsurfaces themselves are not scaled and contain a solid partially transparent red.

Environment variables:
- `CALIBRATION_GRID=1`: Replaces the solid color of the subsurfaces with a calibration grid with 1px features of interchangably partially red and black lines.
- `SCALED_SUBSURFACES=1`: Applies fractional scaling to the subsurfaces as well.

### subsurface-parent-overlap

Creates a surface containing a centered green square. A subsurface filled with red is placed directly on top of the green square to obscure it. This can be used to see if subsurface position and dimension can remain accurate relative to the parent buffer content.

Environment variables:
- `SCALED_PARENT_SURFACE=1`: Applies fractional scaling to the parent surface.
- `SCALED_SUBSURFACES=1`: Applies fractional scaling to the subsurface.

### Ideas for more tests

- Fractional scaled subsurface as child un unscaled surface.
- Nested subsurface positioning, to illustrate accumulated positional errors from certain rounding techniques.
- Input regions/opaque regions.
